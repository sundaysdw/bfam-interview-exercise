# Background

It's an automated Market Making program in a quote-driven market. It responds to client's buy/sell RFQ with a quoted price.

## Assumptions
1. The objective of the market making program is to provide liquidity and try to maintain delta neutral. We thus don't want to lean towards either long or short positions to avoid accumulating them and suffering from loss especially when the market is on up/downtrend. 
2. The program is only designated to one product, i.e. product ID 1
3. The product is a future that has an underlying security without dividend e.g. stocks/indices.
4. The program will mainly focus on responding to buy/sell RFQ with different quoting strategies under different market situations, i.e. up/downtrend and volatility. For simplicity, we would also ignore expiration date and risk-free rate in the future price calculation. That means, we would assume price of future = price of the underlying here.
5. Reference price is the price of the underlying. We have "UnderlyingPriceProvider" to supply us with the prices.

## Quoting Strategies
Base price of the future is based off of the current price (or last traded price) of the underlying.
Each product has a maximum tick that determines the max spread our program can quote. Each product also has a tick size defined.
Under a normal market situation when minimal volatility, our program will give:

* Buy RFQ: base price + 1 tick
* Sell RFQ: base price - 1 tick

There are two strategies in place to reduce risk and try to maintain our positions as neutral as possible:

1. RFQ imbalance

If we found that Buy RFQ is significantly larger than Sell RFQ, we would use it as an indicator that the market is on uptrend where there are significantly more buyers than sellers. To maintain net position zero, we would want to make any incoming Buy RFQ not as attractive, and Sell RFQ more attractive:

* Buy RFQ: base price + 2 tick
* Sell RFQ: base price

Similarly, when the market is on downtrend (sellers > buyers), we would make our buy RFQ more attractive and sell RFQ less attractive.

* Buy RFQ: base price
* Sell RFQ: base price - 2 tick

For Simplicity, we use the total buys and total sells quantity in the last 10 RFQs received to determine the trend.

2. Price historical volatility.

When the market is volatile, it's riskier for market maker to take any of the positions. Thus, we would increase the spread which means higher profit and brings more buffer to counter volatility risk.

* Buy RFQ: base price + (v+1)*tick
* Sell RFQ: base price - (v+1)*tick

where v denotes the volatility (standard deviation of historical prices).


## Testing

Few JUnit tests are created to justify the correctness of quoting, server etc.

1. QuoteCalculationEngineImplTest and UtilsTest: test all quoting scenarios and calculations
2. RFQServerTest: test whether the server is able to respond to multiple client request, with expected quoted prices

## Manually run the program
1. Run RFQServer
2. Run RFQClient
3. Interact with the RFQClient's terminal by giving either RFQ in "{id} {"BUY"/SELL"} {quantity}" format or "quit" to exit
   (Note only security ID 1 is available now)

# Technical Design
## Concurrency
1. Thread Pool:
A Thread Pool of a fixed size (assumed to be 50) is created to handle each incoming client request, which can reduce overhead in creating and destroying threads.
2. ConcurrentHashMap:
ConcurrentHashMap is used for storing im-mem price, product, rfq records, so that the operations can be thread-safe.

## Data storing
In-mem storage is used for simplicity:

1. PriceStore stores historical prices of the security, it subscribes to external price data provider UnderlyingPriceProvider.
2. ProduceStore stores the product info for the security, e.g. expiration date, tick size etc.
3. RFQStore stores historical RFQs we have received.

## Reference

* https://www.informs-sim.org/wsc15papers/027.pdf
* https://medium.datadriveninvestor.com/beginners-guide-to-market-making-with-examples-660977ba440a
* https://www.wikijob.co.uk/content/trading/forex/market-making