package com.example.marketmaker.exception;

/**
 * Exception that indiciates the product does not exist in our database
 */
public class ProductNotFoundException extends RuntimeException{

    public ProductNotFoundException(String msg) {
        super(msg);
    }
}
