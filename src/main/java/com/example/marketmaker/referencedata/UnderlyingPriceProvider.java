package com.example.marketmaker.referencedata;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


public class UnderlyingPriceProvider implements ReferencePriceSource {

    private ConcurrentHashMap<Integer, Double> datastore;

    private List<ReferencePriceSourceListener> listeners = new ArrayList<>();

    public UnderlyingPriceProvider() {
        datastore = new ConcurrentHashMap<>();
    }

    public void onPriceChange(int securityId, double price) {
        listeners.forEach(listener -> listener.referencePriceChanged(securityId, price));
        datastore.put(securityId, price);
    }


    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        listeners.add(listener);
    }

    @Override
    public double get(int securityId) {
        return datastore.getOrDefault(securityId, null);
    }
}
