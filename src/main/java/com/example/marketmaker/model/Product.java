package com.example.marketmaker.model;

/**
 * Abstract class for the final products
 */
public abstract class Product {
    public final static String FUTURES_TYPE = "futures";


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    int id;
    String type;
}
