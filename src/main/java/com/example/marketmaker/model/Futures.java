package com.example.marketmaker.model;

import java.time.LocalDate;

/**
 * Model for Futures
 */
public class Futures extends Product{

    private int id;



    private String type;
    private final double riskRiskRate = 0.0005;
    private LocalDate expiryDate;
    private double tickSize;


    private int maxTick;

    public Futures(int productId, LocalDate expiryDate, double tickSize, int maxTick) {
        this.id = productId;
        this.expiryDate = expiryDate;
        this.tickSize = tickSize;
        this.maxTick = maxTick;
        this.type = Product.FUTURES_TYPE;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }
    public double getRiskRiskRate() {
        return riskRiskRate;
    }
    public double getTickSize() {
        return tickSize;
    }

    public void setTickSize(double tickSize) {
        this.tickSize = tickSize;
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }
    public int getMaxTick() {
        return maxTick;
    }

    public void setMaxTick(int maxTick) {
        this.maxTick = maxTick;
    }
    @Override
    public String getType() {
        return type;
    }
}
