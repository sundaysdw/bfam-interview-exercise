package com.example.marketmaker.model;

/**
 * Model for RFQ
 */
public class RFQ {

    private int securityId;
    private boolean isBuy;
    private int quantity;

    public RFQ() {
    }
    public RFQ(String rawString) {
        String[] parts = rawString.split(" ");
        securityId = Integer.parseInt(parts[0]);
        isBuy = parts[1].equalsIgnoreCase("buy");
        quantity = Integer.parseInt(parts[2]);
    }

    public int getSecurityId() {
        return securityId;
    }

    public boolean isBuy() {
        return isBuy;
    }

    public int getQuantity() {
        return quantity;
    }
}
