package com.example.marketmaker.datastore;

import com.example.marketmaker.model.RFQ;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A store for RFQs received.
 */
public class RFQStore {

    private ConcurrentHashMap<Integer, ArrayList<RFQ>> rfqRecord;

    public RFQStore() {
        this.rfqRecord = new ConcurrentHashMap<>();
    }

    public void addRFQ(int securityId, RFQ rfq) {
        ArrayList<RFQ> record = rfqRecord.getOrDefault(securityId, new ArrayList<>());
        record.add(rfq);
        rfqRecord.put(securityId, record);
    }
    public List<RFQ> getLast10RFQs(int securityId) {
        List<RFQ> rfqsForId =  rfqRecord.getOrDefault(securityId, new ArrayList<>());
        return rfqsForId.subList(Math.max(rfqsForId.size()-10,0), Math.max(rfqsForId.size(), 0));
    }
}
