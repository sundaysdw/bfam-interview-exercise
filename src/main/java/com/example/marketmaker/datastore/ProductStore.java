package com.example.marketmaker.datastore;

import com.example.marketmaker.model.Product;

import java.util.concurrent.ConcurrentHashMap;

/**
 * A store for securities' product information
 */
public class ProductStore {
    private ConcurrentHashMap<Integer, Product> datastore;


    public ProductStore() {
        this.datastore = new ConcurrentHashMap<>();
    }
    public void addProduct(int id, Product product) {
        this.datastore.put(id, product);
    }
    public Product getProduct(int securityId) {
        return datastore.get(securityId);
    }
}
