package com.example.marketmaker.datastore;

import com.example.marketmaker.referencedata.ReferencePriceSource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A store for historical prices of securities
 */
public class PriceStore {
    private ReferencePriceSource referencePriceSource;
    private ConcurrentHashMap<Integer, List<Double>> datastore;


    public PriceStore(ReferencePriceSource source) {
        this.datastore = new ConcurrentHashMap<>();
        source.subscribe((id, price) -> {
            if (!datastore.containsKey(id)) {
                datastore.put(id, new ArrayList<>());
            }
            datastore.get(id).add(price);
        });
        this.referencePriceSource = source;
    }

    public double getPrice(int securityId) {
        if (!datastore.containsKey(securityId)) {
            datastore.put(securityId, new ArrayList<>(List.of(referencePriceSource.get(securityId))));
        }
        List<Double> listOfPrice = datastore.get(securityId);
        return listOfPrice.get(listOfPrice.size()-1);
    }
    public List<Double> getPrices(int securityId) {
        if (!datastore.containsKey(securityId)) {
            datastore.put(securityId, new ArrayList<>(List.of(referencePriceSource.get(securityId))));
        }
        return datastore.get(securityId);
    }

}
