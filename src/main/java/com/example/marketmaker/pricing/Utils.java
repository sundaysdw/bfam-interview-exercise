package com.example.marketmaker.pricing;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Utils class that provides different reusable calculation related methods.
 */
public class Utils {

    public static boolean isUptrend(int buys, int sells) {
        return ((buys - sells) * 1.0 / (buys + sells)) > 0.5;
    }
    public static boolean isDowntrend(int buys, int sells) {
        return ((sells - buys) * 1.0 / (buys + sells)) > 0.5;
    }

    public static double getBuyQuote(double referencePrice, double volatility, int maxTick, double tickSize, int quantity, int additionalTick) {
        return Math.round((referencePrice+(Math.min(Math.round(volatility*maxTick)+additionalTick, maxTick))
                *tickSize)*quantity * 100.0) / 100.0;
    }
    public static double getSellQuote(double referencePrice, double volatility, int maxTick, double tickSize, int quantity, int additionalTick) {
        return Math.round((referencePrice-(Math.min(Math.round(volatility*maxTick)+additionalTick, maxTick))
                *tickSize)*quantity * 100.0) / 100.0;
    }

    public static double calcVolatility(List<Double> prices) {
        List<Double> changes = IntStream
                .range(1, prices.size())
                .mapToDouble(i -> (prices.get(i)/prices.get(i-1))-1)
                .boxed().collect(Collectors.toList());
        double mean = changes.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(Double.NaN);
        double squaredValues = changes.stream()
                .reduce(0.0, (aDouble, aDouble2) -> aDouble + Math.pow(aDouble2-mean, 2));
        return Math.sqrt(squaredValues/(changes.size()-1));
    }
}
