package com.example.marketmaker.pricing;

import com.example.marketmaker.datastore.PriceStore;
import com.example.marketmaker.datastore.ProductStore;
import com.example.marketmaker.datastore.RFQStore;
import com.example.marketmaker.exception.ProductNotFoundException;
import com.example.marketmaker.model.Futures;
import com.example.marketmaker.model.Product;
import com.example.marketmaker.model.RFQ;

import java.util.List;

public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    private final RFQStore rfqStore;
    private final ProductStore productStore;
    private final PriceStore priceStore;

    public QuoteCalculationEngineImpl(RFQStore rfqStore, ProductStore productStore, PriceStore priceStore) {
        this.rfqStore = rfqStore;
        this.productStore = productStore;
        this.priceStore = priceStore;
    }

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        Product targetProduct = productStore.getProduct(securityId);

        if (targetProduct.getType().equalsIgnoreCase(Product.FUTURES_TYPE)) {
            List<RFQ> rfqList = rfqStore.getLast10RFQs(securityId);
            Futures product = (Futures) targetProduct;
            List<Double> historicalPrices = priceStore.getPrices(securityId);
            double volatility = historicalPrices.size() < 10 ? 0.0 : Utils.calcVolatility(historicalPrices);
            if (rfqList.size() == 10) {
                int buys = rfqList.stream().filter(RFQ::isBuy).mapToInt(RFQ::getQuantity).sum();
                int sells =  rfqList.stream().filter(x -> !x.isBuy()).mapToInt(RFQ::getQuantity).sum();

                if (Utils.isUptrend(buys, sells)) {
                    if (buy) {
                        return Utils.getBuyQuote(referencePrice, volatility, product.getMaxTick(),
                                product.getTickSize(), quantity, 2);
                    } else {
                        return Utils.getSellQuote(referencePrice, volatility, product.getMaxTick(),
                                product.getTickSize(), quantity, 0);
                    }
                } else if (Utils.isDowntrend(buys, sells)) {
                    if (!buy) {
                        return Utils.getSellQuote(referencePrice, volatility, product.getMaxTick(),
                                product.getTickSize(), quantity, 2);
                    } else {
                        return Utils.getBuyQuote(referencePrice, volatility, product.getMaxTick(),
                                product.getTickSize(), quantity, 0);
                    }
                }
            }
            // if number of historical RFQ is not large enough or there is no significant trend
            if (buy) {
                return Utils.getBuyQuote(referencePrice, volatility, product.getMaxTick(),
                        product.getTickSize(), quantity, 1);
            } else {
                return Utils.getSellQuote(referencePrice, volatility, product.getMaxTick(),
                        product.getTickSize(), quantity, 1);
            }

        }

        throw new ProductNotFoundException("Product id " + securityId + " not found");

    }




}
