package com.example.marketmaker.connectivity;

import com.example.marketmaker.datastore.PriceStore;
import com.example.marketmaker.datastore.RFQStore;
import com.example.marketmaker.model.RFQ;
import com.example.marketmaker.pricing.QuoteCalculationEngineImpl;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

/**
 * Handler for RFQ. One is created for each TCP client. Responsible for handling every interaction with the client.
 */
@Slf4j
public class RFQHandler implements Runnable {

    private final Socket socket;
    private final QuoteCalculationEngineImpl quoteCalculationEngine;
    private final PriceStore localPriceStore;
    private final RFQStore rfqStore;
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("hh:mm:ss");

    public RFQHandler(Socket socket, QuoteCalculationEngineImpl quoteCalculationEngine, PriceStore priceStore, RFQStore rfqStore) {
        this.socket = socket;
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.localPriceStore = priceStore;
        this.rfqStore = rfqStore;
    }

    @Override
    public void run() {
        while (true) {
            try {
                InputStream input = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                OutputStream output = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(output, true);

                String rawRfq = reader.readLine();

                if (rawRfq.equalsIgnoreCase("quit")) {
                    log.info("Client disconnected");
                    break;
                }
                Instant before = Instant.now();

                log.info("Received quote from client {} at {}", rawRfq, SIMPLE_DATE_FORMAT.format(new Date(System.currentTimeMillis())));
                RFQ rfq = new RFQ(rawRfq);

                writer.println(quoteCalculationEngine.calculateQuotePrice(
                        rfq.getSecurityId(), localPriceStore.getPrice(rfq.getSecurityId()),
                        rfq.isBuy(), rfq.getQuantity()
                ));
                Instant after = Instant.now();
                long delta = Duration.between(before, after).toMillis();
                log.info("Finished processing client request in {} ms", delta);

                rfqStore.addRFQ(rfq.getSecurityId(), rfq);

            } catch (Exception ex) {
                log.error("Error occurred while handling RFQ {}", ex.getMessage());
                break;
            }
        }
        try {
            if (socket != null) {
                socket.close();
                log.error("Closed connection with client");

            }
        } catch (IOException ie){
            log.error("Error occurred while closing connection with client");
        }



    }
}
