package com.example.marketmaker.connectivity;

import com.example.marketmaker.datastore.ProductStore;
import com.example.marketmaker.datastore.RFQStore;
import com.example.marketmaker.model.Futures;
import com.example.marketmaker.pricing.QuoteCalculationEngineImpl;
import com.example.marketmaker.datastore.PriceStore;
import com.example.marketmaker.referencedata.UnderlyingPriceProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * TCP server for handling client connections and responding with quoted prices.
 */
@Slf4j
public class RFQServer {
    static final int MAX_THREAD = 50;


    private QuoteCalculationEngineImpl quoteCalculationEngine;
    private PriceStore priceStore;
    private RFQStore rfqStore;


    public RFQServer(RFQStore rfqStore, ProductStore productStore, PriceStore priceStore) {
        this.rfqStore = rfqStore;
        this.priceStore = priceStore;
        this.quoteCalculationEngine = new QuoteCalculationEngineImpl(rfqStore, productStore, priceStore);
    }

    public void start() {
        ExecutorService pool = Executors.newFixedThreadPool(MAX_THREAD);
        ServerSocket ss = null;
        try {

            ss = new ServerSocket(8080);
            log.info("Server started at {}:{}", ss.getInetAddress(), ss.getLocalPort());
            while (true) {
                Socket s = ss.accept();
                log.info("New client connected " + s.getInetAddress().getHostAddress());
                RFQHandler rfqHandler = new RFQHandler(s, quoteCalculationEngine, priceStore, rfqStore);
                pool.execute(rfqHandler);
            }
        } catch (IOException e) {
            log.error("Server Exception: {}", e.getMessage());
        } finally {
            pool.shutdown();
            if (ss != null) {
                try {
                    ss.close();
                    log.info("Server closed");
                } catch (IOException ie) {
                    log.error("Error occurred while closing server {}", ie.getMessage());
                }
            }
        }
    }

    public static void main(String[] args) {
        UnderlyingPriceProvider priceProvider = new UnderlyingPriceProvider();
        PriceStore priceStore = new PriceStore(priceProvider);
        priceProvider.onPriceChange(1, 10.0);
        priceProvider.onPriceChange(2, 100.0);

        RFQStore rfqStore = new RFQStore();
        ProductStore productStore = new ProductStore();
        productStore.addProduct(1, new Futures(1, LocalDate.now().plusDays(30), 0.01, 5));
        productStore.addProduct(2, new Futures(2, LocalDate.now().plusDays(60), 0.1, 5));

        RFQServer server = new RFQServer(rfqStore, productStore, priceStore);
        server.start();
    }
}
