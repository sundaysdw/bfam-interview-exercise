package com.example.marketmaker.connectivity;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * TCP client program for sending RFQ to server
 */
@Slf4j
public class RFQClient {

    Socket socket;

    public Double sendRFQ(String request) {
        double quote;
        if (socket != null) {
            try {
                OutputStream output = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(output, true);
                writer.println(request);
                log.info("Sending RFQ {} to server...", request);
                InputStream input = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));

                quote = Double.parseDouble(reader.readLine());
                log.info("Received quote price {}", quote);
                return quote;
            } catch (Exception ex) {
                log.error("Error occurs while sending RFQ to server: {}", ex.getMessage());
            }
        } else {
            log.error("Error sending RFQ to server. Socket is not init yet");
        }
        return null;
    }

    public void connect() {
        log.info("Establishing connection with Server...");
        try {
            socket = new Socket("localhost", 8080);
            log.info("Connection with Server is successfully established");
        } catch (Exception ex) {
            log.error("Error occurred when trying establish connectivity with server: {}", ex.getMessage());
        }
    }
    public void close() {
        try {
            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);
            writer.println("quit");
            log.info("Disconnecting from server...");

        } catch (IOException exc) {
            log.error("Error occurred when closing connection with RFQ Server");
        } finally {
            try {
                socket.close();
                log.info("Disconnected with server");
            } catch (IOException exc) {
                log.error("Error occurred when closing connection with RFQ Server");
            }
        }
    }
    public static void main(String[] args) {

        RFQClient client = new RFQClient();

        Scanner scanner = new Scanner(System.in);
        String input;

        client.connect();
        while (true) {
            System.out.println("Send your RFQ in format {id} {\"BUY\"/\"SELL\"} {quantity}: ");
            input = scanner.nextLine();
            if (input.equalsIgnoreCase("quit")) {
                break;
            }
            if (input.split(" ").length != 3) {
                System.out.println("Please send your RFQ in correct format.");
            } else {
                Double quotePrice = client.sendRFQ(input);
                if (quotePrice == null){
                    System.out.println("Quote price is not available");
                } else {
                    System.out.println("Quote price received: " + quotePrice);
                }
            }
        }
        client.close();

    }
}
