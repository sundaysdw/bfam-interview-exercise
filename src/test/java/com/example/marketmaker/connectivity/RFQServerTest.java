package com.example.marketmaker.connectivity;

import com.example.marketmaker.datastore.PriceStore;
import com.example.marketmaker.datastore.ProductStore;
import com.example.marketmaker.datastore.RFQStore;
import com.example.marketmaker.model.Futures;
import com.example.marketmaker.referencedata.UnderlyingPriceProvider;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import static org.junit.Assert.*;


public class RFQServerTest {


    /**
     * Starting up the server with some historical prices in record.
     * @throws InterruptedException
     */
    @Before
    public void setUp() throws InterruptedException {
        UnderlyingPriceProvider priceProvider = new UnderlyingPriceProvider();
        PriceStore priceStore = new PriceStore(priceProvider);
        priceProvider.onPriceChange(1, 10.0);
        priceProvider.onPriceChange(1, 20.0);
        priceProvider.onPriceChange(1, 30.0);
        priceProvider.onPriceChange(1, 40.0);
        priceProvider.onPriceChange(1, 50.0);
        priceProvider.onPriceChange(1, 60.0);
        priceProvider.onPriceChange(1, 70.0);
        priceProvider.onPriceChange(1, 80.0);
        priceProvider.onPriceChange(1, 90.0);
        priceProvider.onPriceChange(1, 100.0);

        RFQStore rfqStore = new RFQStore();
        ProductStore productStore = new ProductStore();
        productStore.addProduct(1, new Futures(1, LocalDate.now().plusDays(30), 0.01, 5));

        new Thread(() -> {
            RFQServer server = new RFQServer(rfqStore, productStore, priceStore);
            server.start();
        }).start();

        Thread.sleep(3*1000);

    }

    /**
        Test case description:
        1. Client sends 10 times "1 BUY 5"
        2. Client1 sends "1 BUY 1"
        3. Client2 sends "1 SELL 1"
        4. Client3 sends "1 BUY 10"
     */
    @Test
    public void multiClientTest() {
        RFQClient client = new RFQClient();
        client.connect();
        for (int i=1; i<10; i++) {
            double quote = client.sendRFQ("1 BUY 5");
            assertEquals(500.1, quote, 0.001);
        }

        RFQClient client1 = new RFQClient();
        RFQClient client2 = new RFQClient();
        RFQClient client3 = new RFQClient();

        client1.connect();
        double quote = client1.sendRFQ("1 BUY 1");
        assertEquals(100.02, quote, 0.001);
        client2.connect();
        double quote2 = client2.sendRFQ("1 SELL 1");
        assertEquals(99.99, quote2, 0.001);
        client3.connect();
        double quote3 = client3.sendRFQ("1 BUY 10");
        assertEquals(1000.3, quote3, 0.001);
        client.close();
        client1.close();
        client2.close();
        client3.close();
    }

}