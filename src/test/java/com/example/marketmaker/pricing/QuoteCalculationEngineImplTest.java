package com.example.marketmaker.pricing;

import com.example.marketmaker.datastore.PriceStore;
import com.example.marketmaker.datastore.ProductStore;
import com.example.marketmaker.datastore.RFQStore;
import com.example.marketmaker.model.Futures;
import com.example.marketmaker.model.RFQ;
import com.example.marketmaker.referencedata.UnderlyingPriceProvider;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class QuoteCalculationEngineImplTest {
    private RFQStore rfqStore = new RFQStore();
    private ProductStore productStore = new ProductStore();
    UnderlyingPriceProvider priceProvider = new UnderlyingPriceProvider();
    private PriceStore priceStore = new PriceStore(priceProvider);
    QuoteCalculationEngineImpl engine = new QuoteCalculationEngineImpl(rfqStore, productStore, priceStore);

    @Before
    public void setUp() {
        productStore.addProduct(1, new Futures(1,LocalDate.now().plusDays(30), 0.01, 5));
        priceProvider.onPriceChange(1, 10.0);
    }

    @Test
    public void calculateQuotePriceWithSimpleBuyStrategy() {
        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 5;
        double result = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(50.05, result, 0.0);
    }

    @Test
    public void calculateQuotePriceWithSimpleSellStrategy() {
        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 5;
        double result = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(49.95, result, 0.0);
    }

    @Test
    public void calculateQuotePriceWithUpTrendStrategy() {
        rfqStore.addRFQ(1, new RFQ("1 BUY 600"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 1;
        double result = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(10.02, result, 0.0);

        double result2 = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(10.0, result2, 0.0);
    }
    @Test
    public void calculateQuotePriceWithDownTrendStrategy() {
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 600"));
        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 1;
        double result = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(9.98, result, 0.0);

        double result2 = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(10.0, result2, 0.0);
    }
    @Test
    public void calculateQuotePriceWithNoTrendStrategy() {
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 1;
        double result = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(9.99, result, 0.0);

        double result2 = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(10.01, result2, 0.0);
    }
    @Test
    public void calculateQuotePriceWithVolatilityStrategy() {
        priceProvider.onPriceChange(1, 20.0);
        priceProvider.onPriceChange(1, 30.0);
        priceProvider.onPriceChange(1, 40.0);
        priceProvider.onPriceChange(1, 50.0);
        priceProvider.onPriceChange(1, 60.0);
        priceProvider.onPriceChange(1, 70.0);
        priceProvider.onPriceChange(1, 80.0);
        priceProvider.onPriceChange(1, 90.0);
        priceProvider.onPriceChange(1, 100.0);

        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 1;
        double result = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(9.98, result, 0.0);

        double result2 = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(10.02, result2, 0.0);

    }
    @Test
    public void calculateQuotePriceWithMaxVolatilityStrategy() {

        priceProvider.onPriceChange(1, 20.0);
        priceProvider.onPriceChange(1, 10.0);
        priceProvider.onPriceChange(1, 30.0);
        priceProvider.onPriceChange(1, 40.0);
        priceProvider.onPriceChange(1, 20.0);
        priceProvider.onPriceChange(1, 10.0);
        priceProvider.onPriceChange(1, 20.0);
        priceProvider.onPriceChange(1, 5.0);
        priceProvider.onPriceChange(1, 10.0);

        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 1;
        double result = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(9.95, result, 0.0);

        double result2 = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(10.05, result2, 0.0);

    }
    @Test
    public void calculateQuotePriceWithDownTrendAndVolatilityStrategy() {
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 600"));

        priceProvider.onPriceChange(1, 20.0);
        priceProvider.onPriceChange(1, 30.0);
        priceProvider.onPriceChange(1, 40.0);
        priceProvider.onPriceChange(1, 50.0);
        priceProvider.onPriceChange(1, 60.0);
        priceProvider.onPriceChange(1, 70.0);
        priceProvider.onPriceChange(1, 80.0);
        priceProvider.onPriceChange(1, 90.0);
        priceProvider.onPriceChange(1, 100.0);

        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 1;
        double result = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(9.97, result, 0.0);

        double result2 = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(10.01, result2, 0.0);

    }
    @Test
    public void calculateQuotePriceWithUpTrendAndVolatilityStrategy() {
        rfqStore.addRFQ(1, new RFQ("1 BUY 600"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 BUY 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));
        rfqStore.addRFQ(1, new RFQ("1 SELL 10"));

        priceProvider.onPriceChange(1, 20.0);
        priceProvider.onPriceChange(1, 30.0);
        priceProvider.onPriceChange(1, 40.0);
        priceProvider.onPriceChange(1, 50.0);
        priceProvider.onPriceChange(1, 60.0);
        priceProvider.onPriceChange(1, 70.0);
        priceProvider.onPriceChange(1, 80.0);
        priceProvider.onPriceChange(1, 90.0);
        priceProvider.onPriceChange(1, 100.0);

        int securityId = 1;
        double referencePrice = 10.0;
        int quantity = 1;
        double result = engine.calculateQuotePrice(securityId, referencePrice, false, quantity);
        assertEquals(9.99, result, 0.0);

        double result2 = engine.calculateQuotePrice(securityId, referencePrice, true, quantity);
        assertEquals(10.03, result2, 0.0);

    }
}
