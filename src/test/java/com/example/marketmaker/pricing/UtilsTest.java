package com.example.marketmaker.pricing;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;

public class UtilsTest {
    @Test
    public void isUptrend() {
        assertFalse(Utils.isUptrend(100,200));
        assertFalse(Utils.isUptrend(200,200));
        assertTrue(Utils.isUptrend(800,200));
    }

    @Test
    public void isDowntrend() {
        assertTrue(Utils.isDowntrend(100,400));
        assertFalse(Utils.isDowntrend(200,200));
        assertFalse(Utils.isDowntrend(400,200));
    }

    @Test
    public void getBuyPrice() {
        assertEquals(1000.1, Utils.getBuyQuote(100.0, 0.2, 5,
                0.01, 10, 0), 0.00);
        assertEquals(1000.3, Utils.getBuyQuote(100.0, 0.15, 5,
                0.01, 10, 2), 0.00);
        assertEquals(200.0, Utils.getBuyQuote(100.0, 0, 5,
                0.01, 2, 0), 0.00);

    }
    @Test
    public void getSellPrice() {
        assertEquals(999.9, Utils.getSellQuote(100.0, 0.2, 5,
                0.01, 10, 0), 0.00);
        assertEquals(999.7, Utils.getSellQuote(100.0, 0.15, 5,
                0.01, 10, 2), 0.00);
        assertEquals(200.0, Utils.getSellQuote(100.0, 0, 5,
                0.01, 2, 0), 0.00);
    }

    @Test
    public void calcVolatility() {
        ArrayList<Double> prices = new ArrayList<>();
        prices.add(1.0);
        prices.add(2.0);
        prices.add(3.0);
        prices.add(4.0);
        prices.add(5.0);
        assertEquals(0.3359, Utils.calcVolatility(prices), 0.0001);

        ArrayList<Double> prices2 = new ArrayList<>();
        prices2.add(10.0);
        prices2.add(20.0);
        prices2.add(30.0);
        prices2.add(40.0);
        prices2.add(50.0);
        prices2.add(60.0);
        prices2.add(70.0);
        prices2.add(80.0);
        prices2.add(90.0);
        prices2.add(100.0);
        assertEquals(0.2851, Utils.calcVolatility(prices2), 0.0001);
    }
}
